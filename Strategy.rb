require_relative 'Hand.rb'
require_relative 'Card.rb'

##
# Various methods managing the strategy of the players

# Dealer hits on 16 and below and stands on 17+
def dealer_strategy(hand)
	if hand.value > 16
		return :stand
	else
		return :hit
	end
end

# Unimplemented currently (just copies dealer strategy).
# If I have the time, I mean to code one of the simple casino strategies.
# Method is called when player asks for a hint
def optimal_strategy(hand, moneyToBet = 1000)
	if hand.value > 16
		return :stand
	else
		return :hit
	end
end

# Gives the player info about her hand through stdout and asks for a decision
def human_strategy(hand, moneyToBet = 1000) # Amount of money is relevant when determining if split/double down is possible
	puts hand

	if hand.splittable? and moneyToBet >= hand.getBet
		puts "Split hand? (Y/N) Bet to split is " + hand.getBet.to_s
		input = gets.chomp
		if input == 'Y' or input == 'y'
			return :split
		end
	end
			
	puts '(H) to Hit, (S) to Stand, (D) to double down. (hint) for computer move.'
	decision = gets.chomp.upcase
	case decision
	when 'H'
		return :hit
	when 'S'
		return :stand
	when 'D'
		if hand.size == 2
			return :doubleDown
		else
			puts "Can only double down on 2-card hands!"
			return human_strategy(hand)	# Given time, may consider reformulating this to not use recursion, unless Ruby supports tail call optimization.
		end
	when 'HINT'
		return optimal_strategy(hand, moneyToBet)
	else
		puts "Improper input"
		return human_strategy(hand, moneyToBet)		# Ditto regarding future removal of recursion.
	end
end