require_relative 'Hand.rb'
require "test/unit"

class TestHand < Test::Unit::TestCase

	def test_hand_values
		six = Card.new(5)
		seven = Card.new(6)
		ace = Card.new(0)
		king = Card.new(12)
		queen = Card.new(11)
		four = Card.new(3)
		
		hand = Hand.new
		
		hand.addCard(six)
		assert_equal(6, hand.value)
		hand.addCard(ace)
		assert_equal(17, hand.value)
		hand.addCard(four)
		assert_equal(21, hand.value)
		assert_equal(false, hand.bust?)
		assert_equal(false, hand.blackjack?)
		
		hand.addCard(seven)
		assert_equal(18, hand.value)
		assert_equal(false, hand.bust?)
		
		hand.addCard(queen)
		assert_equal(28, hand.value)
		assert_equal(true, hand.bust?)
		
		hand2 = Hand.new
		hand2.addCard(king)
		hand2.addCard(ace)
		
		assert_equal(21, hand2.value)
		assert_equal(true, hand2.blackjack?)
	end
	
	def test_payouts
		six = Card.new(5)
		seven = Card.new(6)
		ace = Card.new(0)
		king = Card.new(12)
		queen = Card.new(11)
		four = Card.new(3)
		
		hand = Hand.new
		
		hand.setBet(100)
		
		assert_equal(200, hand.win_payout)
		assert_equal(100, hand.tie_payout)
		hand.addCard(king)
		hand.addCard(ace)
		assert_equal(250, hand.win_payout)
		
	end
	
	def test_splits
		six = Card.new(5 + 13)
		seven = Card.new(6 + 52)
		ace = Card.new(0 + 26)
		king = Card.new(12 + 39)
		seven2 = Card.new(6 + 13)
		queen = Card.new(11)
		queen2 = Card.new(11 + 13)
		
		hand = Hand.new
		
		assert_equal(false, hand.splittable?)
		hand.addCard(seven)
		assert_equal(false, hand.splittable?)
		hand.addCard(seven2)
		assert_equal(true, hand.splittable?)
		
		hand2 = Hand.new
		hand2.addCard(king)
		hand2.addCard(queen)
		assert_equal(true, hand2.splittable?)
		
		hand3 = hand2.split
		
		assert_equal(false, hand2.splittable?)
		
		hand3.addCard(queen2)
		assert_equal(true, hand3.splittable?)
		
	end
		
	
end
		