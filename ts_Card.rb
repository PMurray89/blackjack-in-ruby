require_relative 'Card.rb'
require "test/unit"

class TestCard < Test::Unit::TestCase

	def test_card_strings
		nine = Card.new(8 + 0*52)
		king = Card.new(12 + 1*52)
		ace = Card.new(0+2*52)
		ten = Card.new(9 + 3*52)
		assert_equal(true, ace.ace?)
		assert_equal(false, ten.ace?)
		
		assert_equal('9 of spades', nine.to_s)
		assert_equal('King of spades', king.to_s)
		assert_equal('Ace of diamonds', ace.to_s)
		assert_equal('10 of clubs', ten.to_s)
	end
	
	def test_card_value
		six1 = Card.new(5)
		six2 = Card.new(5 + 2*52)
		ace1 = Card.new(0)
		ace2 = Card.new(3*52)
		
		queen = Card.new(11)
		jack = Card.new(10)
		
		three = Card.new(2)
		
		assert_equal(6, six1.value)
		assert_equal(6, six2.value)
		
		assert_equal(11, ace1.value)
		assert_equal(11, ace2.value)
		
		assert_equal(10, queen.value)
		assert_equal(10, jack.value)
		
		assert_equal(3, three.value)
	end
	
end
		