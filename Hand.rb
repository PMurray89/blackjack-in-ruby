require_relative 'Card.rb'
## 
# Hand class organizes the cards in a hand and keeps track of the bet wagered
# on each hand
class Hand
	def initialize(came_from_split = :unsplit)
		@cards = Array.new
		if came_from_split == :split
			@previouslySplit = true
		else
			@previouslySplit = false
		end
		@bet = 0
	end
	
	# set the current bet
	def setBet(bet)
		@bet = bet
	end
	
	# see the current bet
	def getBet
		return @bet
	end
	
	# How much does this hand pay out upon winning
	def win_payout
		if self.blackjack?
			return (2.5*@bet).to_i	# may round down (I guess it just contributes to the house edge)
		else
			return 2*@bet
		end
	end
	
	# How much should be returned to player from a tie
	def tie_payout
		return @bet
	end
	
	# Insert a card into the hand
	def addCard(card)
		@cards.push(card)
	end
	
	# return total value of hand
	def value
		totalValue = 0
		softAces = 0
		# Find the maximum total value, but keep track of the number of aces
		@cards.each do |card|
			totalValue += card.value
			if card.value == 11
				softAces += 1
			end
		end
		# If totalValue would be a bust, convert available soft aces from 11 pts to 1 pt
		while softAces > 0 and totalValue > 21
			totalValue -= 10
			softAces -= 1
		end
		return totalValue	# total value is either the largest possible hand value at/below 21, or the smallest possible hand value (over 21)
	end
	
	# Test if this hand can be split
	def splittable?
		if @cards.size == 2 and @cards[0].value == @cards[1].value
			return true
		else
			return false
		end
	end
	
	# Returns the new hand formed from a split
	def split
		if not self.splittable?
			abort("Can't split this hand:\n" + self.to_s)
		end
		newHand = Hand.new(:split)	# :split input marks a hand as split
		@previouslySplit = true		# marks hand as split preventing bonus blackjack payout and A+10 beating other 21 pt hands
		card = @cards.pop
		newHand.addCard(card)
		return newHand
	end		
	
	# see top card of hand (for dealer)
	def getTopCard
		return @cards[0]
	end
	
	# decide if hand has busted
	def bust?
		return self.value > 21
	end
	
	
	# decide if hand is a blackjack, and thus eligible for larger winnings and beating other equal value hands
	def blackjack?
		return (self.value == 21 and @cards.size == 2 and not @previouslySplit)
	end
	
	# Empties hand
	def returnToShoe
		@cards.clear
	end

	# return number of cards in hand
	def size
		return @cards.size
	end
	
	# print string representation of hand's cards and total value
	def to_s
		return "(Hand Value: #{self.value}) " + @cards.collect(&:to_s).join(",\t")
		# Returns a string describing the relevant parts of the hand. Will look like the following
		# "(Hand Value: 18) Ace of hearts,	7 of spades"
	end
	
end