require_relative 'Hand.rb'
require_relative 'Card.rb'
require_relative 'Strategy.rb'

# Since the dealer differs from a normal player quite considerably (no money pool, no splitting, no double down, very simple strategy), I decided to not have Dealer inherit from Player or vice versa

##
# This class represents the dealer in a blackjack game. It is essentially just a wrapper around
# one hand, since the dealer has exactly one hand and a limitless bankroll.
class Dealer
	# Initialize an empty hand
	def initialize
		@hand = Hand.new
	end
	
	# Reset dealer's hand to two new cards
	def createNewHand(card1, card2)
		@hand.returnToShoe
		@hand.addCard(card1)
		@hand.addCard(card2)
	end
	
	# Check if dealer should offer insurance
	def offerInsurance?
		return @hand.getTopCard.ace?
	end
	
	# Check if dealer busted
	def bust?
		return @hand.bust?
	end
	
	# Return value of dealer's hand
	def handValue
		return @hand.value
	end
	
	# Decide dealer's next move
	def decideMove
		return dealer_strategy(@hand)
	end
	
	# Dealer's way of hitting
	def getCard(card)
		@hand.addCard(card)
	end
	
	# Return string describing dealer's hand
	def showHand
		return @hand.to_s
	end
	
	# Check if dealer has blackjack
	def blackjack?
		return @hand.blackjack?
	end
	
	# Print initial dealer status
	def to_s
		return "\nDealer: " + @hand.getTopCard.to_s
	end
	
end
			