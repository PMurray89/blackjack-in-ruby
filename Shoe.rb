require_relative 'Card.rb'

##
# This class holds all the cards used in the game. It is initialized by a number 
# of decks inserted into the shoe
class Shoe
	def initialize(number_of_decks = 1)
		@numCards = 52*number_of_decks
		@cardArray = Array.new(@numCards, Card.new)
		(0..(@numCards - 1)).each do |num|
			@cardArray[num] = Card.new(num)
		end
		@cardArray.shuffle!	# shuffle the deck upon initialization
		@nextCardIndex = 0
	end
	
	# shuffles the cards in the shoe
	def shuffleDecks!
		@cardArray.shuffle!
		@nextCardIndex = 0
	end
	
	# draw a card from the shoe
	def drawCard
		if @nextCardIndex >= @numCards
			abort("We ran out of cards!")	# This should never happen in a casino game
		end
		@nextCardIndex +=  1
		return @cardArray[@nextCardIndex - 1]
	end
	
	# Return the number of total cards in the shoe
	def numberOfCards
		return @numCards
	end
	
	# Return number of cards left in the shoe. Unused.
	def remainingCards
		return @numCards - @nextCardIndex
	end
	
	# Print all the cards in order remaining in the shoe.
	def to_s
		return "Shoe contains the following cards:\n#{@cardArray[@nextCardIndex .. -1].collect(&:to_s).join "\n"}"
	end
end
