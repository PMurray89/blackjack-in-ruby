##
# This class represents a playing card in a standard 52 card deck
class Card
	# SUITES hash defines how to print suites.
	@@SUITES = {:hearts => 'hearts', :clubs => 'clubs', :diamonds => 'diamonds', :spades => 'spades'}

	# Initialize a new card based on an integer
	def initialize(number = 0)
		number = number.to_i
		@rank = number % 13 + 1
		case number % 4
		when 0
			@suite = :spades
		when 1
			@suite = :hearts
		when 2
			@suite = :diamonds
		when 3
			@suite = :clubs
		end
	end
	
	# Check if card is an ace
	def ace?
		if @rank == 1
			return true
		else
			return false
		end
	end
	
	# Return value of card
	def value()
		case @rank
		when 2..10
			return @rank
		when 11..13
			return 10
		when 1
			return 11
		end
	end
	
	# Return string describing the card
	def to_s
		case @rank
		when 2..10
			return "#@rank of #{@@SUITES[@suite]}"
		when 11
			return "Jack of #{@@SUITES[@suite]}"
		when 12
			return "Queen of #{@@SUITES[@suite]}"
		when 13
			return "King of #{@@SUITES[@suite]}"
		when 1
			return "Ace of #{@@SUITES[@suite]}"
		end
	end
end