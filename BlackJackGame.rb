require_relative 'Player.rb'
require_relative 'Dealer.rb'
require_relative 'Hand.rb'
require_relative 'Strategy.rb'
require_relative 'Shoe.rb'
require_relative 'Card.rb'

##
# This class manages and runs the players, hands, dealer and shoe (decks of cards)
# in a typical blackjack game.

class BlackJackGame

	# Create a game with no players and a specified number of decks
	def initialize(numDecks = 6)
		@dealer = Dealer.new
		@shoe = Shoe.new(numDecks)
		@players = Array.new
	end
	
	# Add a player with a given name to the game
	def addPlayer(newPlayerName)
		# A little insurance to make sure we do not run out of cards. 
		# On average, players will use less than 5 cards each. Plus card counting becomes
		# a bigger danger as the decks : players ratio shrinks
		if 5*@players.size > @shoe.numberOfCards
			puts "Too many players already!"
		else
			@players.push(Player.new(newPlayerName))
		end
	end
	
	# Remove any players who have no money from the game.
	def removeBrokePlayers
		players_to_remove = Array.new
		@players.each do |player|
			if player.bankroll <= 0
				puts "\n" + player.name + " ran out of money!"
				puts player.name + " is out of the game."
				players_to_remove.push(player)
			end
		end
		# Ran into an issue where removing players inside the loop messed
		# up the iteration and caused some players to be missed.
		players_to_remove.each do |player|
			@players.delete(player)
		end
	end
	
	# Ask every player to make all actions required to fill their hands with cards,
	# including hitting, standing, doubling down, and splitting.
	def fillPlayerHands
		@players.each do |player|
			puts "\n" + player.to_s + "'s turn. $" + player.bankroll.to_s + " available to bet.\n"
			player.eachHand do |hand|
				sleep(1.0)
				if hand.blackjack?
					puts 'BlackJack!'
					puts hand
				end
				while hand.value < 21
					puts "\n"
					case player.decideMove(hand)
					when :doubleDown	# double down means to double your bet and take exactly one more card
						player.doubleBet(hand)
						card = @shoe.drawCard
						hand.addCard(card)
						puts "#{player.name} doubled down. Received " + card.to_s + '.' 
						if hand.bust?
							puts player.to_s + "'s hand busted! lost $" + hand.getBet.to_s
						end							
						break	
					when :hit	# hit means to request another card
						card = @shoe.drawCard
						hand.addCard(card)
						puts "#{player.name} hit. Received " + card.to_s + '.'
						if hand.bust?
							puts player.to_s + "'s hand busted! lost $" + hand.getBet.to_s
						end
					when :stand	# stand means to keep hand as is and end the turn
						puts "#{player.name} stood with a total value of " + hand.value.to_s
						break
					when :split	# split makes two hands from one, and puts an equal bet on each
						player.splitHand(hand)
						puts "#{player.name} split your hand!"
					else
						abort('invalid action')
					end
				end
				sleep(1.0)
			end
		end
	end
	
	# End the current round. Should only be called within playRound. Displays player
	# money totals, allows players to leave, and removes any broke players.
	def endRound
			sleep(3.0/2.0)
			puts "\nAfter this round, current totals are:\n"
			players_who_left = Array.new
			@players.each do |player|
				puts player.name + ": $" + player.bankroll.to_s
				# Offer players with money the chance to leave
				if player.bankroll != 0
					puts "Do you want to leave the game now, #{player.name}? (Y/N)"
					input = gets.chomp.upcase
					if input == 'Y'
					players_who_left.push(player)
						if player.bankroll > 1000
							puts player.name + " left the table with $#{player.bankroll - 1000} in winnings!"
						elsif player.bankroll < 1000
							puts player.name + " left the table after losing $#{1000 - player.bankroll}!"
						else
							puts player.name + " left the table with even money"
						end
					end
				end
				sleep(1.0/2.0)
			end
			# Removing players within loop caused some issues
			players_who_left.each do |player|
				@players.delete(player)
			end
			
			# Remove all bankrupt players
			self.removeBrokePlayers
		return true
	end
	
	# Completes one cycle of play. It returns true if a real cycle occurred, and false if
	# the table is empty of human players.
	def playRound
	
		if @players.size == 0
			puts "Nobody is at the table!"
			return false
		end
		
		@shoe.shuffleDecks!
		
		@players.each do |player| 
		# A rather ugly while loop to allow for repeated attempts
			maxWager = player.bankroll
			if maxWager <= 0
				# Should be unreachable now due to removeBrokePlayers method
				puts "Player " + player.to_s + " is out of money and has left the game.\n"
				next
			end
			bet = maxWager + 1
			while bet > maxWager or bet < 1
				puts "\n" + player.to_s + ", choose your wager. You have $" + player.bankroll.to_s + "."
				bet = gets.chomp.to_i
				if bet > maxWager	# Player tried to bet more than she has
					puts "\nA $#{bet} is too high; you only have $#{maxWager}!\n"
				end
				if bet < 1
					puts "\nYour bet must be greater than zero!\n"
				end
			end
			player.createFirstHand(@shoe.drawCard, @shoe.drawCard, bet)
		end
				
		@dealer.createNewHand(@shoe.drawCard, @shoe.drawCard)
		print "\n"
		puts @dealer	# print dealer's Up Card
		
		# Print initial hands so everyone has max card counting knowledge
		@players.each do |player|
			print player.name + "'s hand:\n\t"
			player.eachHand do |hand|
				puts hand
			end
			sleep(3.0/2.0)
		end
		
		# If the dealer's top card is an Ace, insurance is offered to each player
		if (@dealer.offerInsurance?)
			@players.each do |player|
				player.offerInsurance
			end
		end
		
		# Game begins by dealer checking for blackjack. If he has it, all bets are lost, 
		# except those from player blackjacks, which are pushed.
		if @dealer.blackjack?
			puts "Dealer blackjack!!"
			@players.each do |player|
				player.eachHand do |hand|
				# Note, hands do not need to be explicitly lost. The money is already in the hand's bet, and will then be reset when hands are reinitialized
					if not hand.blackjack?
						puts player.to_s + " lost $" + hand.getBet.to_s + " to dealer blackjack."
					else
						puts player.to_s + " tied with dealer blackjack."
					end
				end
				player.payoutInsurance	# Any player with an insurance bet will win some money back
			end
			return self.endRound	# End round at this point
		else
			puts "No dealer blackjack!"
		end
		
		# Let each player play out his hand
		self.fillPlayerHands
		
		puts "\nDealer's hand:\n" + @dealer.showHand
		
		
		# Dealer's decisions to hit/stand are made here
		while not @dealer.bust?
			sleep(3.0/2.0)	# Delays added in to make game more readable
			case @dealer.decideMove
			when :hit
				card = @shoe.drawCard
				print card
				@dealer.getCard(card)
				puts "\t (Value now #{@dealer.handValue})"
				if @dealer.bust?
					puts "Dealer busted!"
				end
			when :stand
				break
			else	# Shouldn't reach, but perhaps dealer_strategy will be updated in future
				abort('invalid dealer action')
			end
		end
		
		# When dealer busts, all active player hands win
		if @dealer.bust?
			@players.each do |player|
			num = 1
				player.eachHand do |hand|
					if not (hand.bust? or hand.blackjack?)
						sleep(3.0/2.0)
						puts player.to_s + "'s hand \##{num} won $#{hand.getBet}!"
						player.winHand(hand)
					elsif hand.blackjack?
						sleep(3.0/2.0)
						puts player.name + "'s hand \##{num} won $#{(1.5*hand.getBet).to_i}!"
						player.winHand(hand)
					end
					num += 1
				end
			end
		else	# otherwise the dealer did not bust
			puts "\nDealer's final hand:\n" + @dealer.showHand
			@players.each do |player|
				num = 1
				player.eachHand do |hand|
					unless hand.bust?	# if hand busted, we already reported the loss
						if hand.blackjack?	# Set multiplier for reporting winnings
							multiplier = 1.5
						else
							multiplier = 1
						end
						sleep(3.0/2.0)
						if hand.value > @dealer.handValue or (hand.blackjack? and not @dealer.blackjack?)
							puts player.to_s + "'s hand \##{num} won $#{(multiplier*hand.getBet).to_i}!"
							player.winHand(hand)
						elsif hand.value == @dealer.handValue and not @dealer.blackjack?
							puts player.name + "'s hand \##{num} tied. Returned $#{hand.getBet}!"
							player.tieHand(hand)
						else		# otherwise hands's value < dealer or dealer got blackjack and you did not
							puts player.to_s + "'s hand \##{num} lost $#{hand.getBet}!"
						end
					end
					num += 1
				end
			end
		end

		return self.endRound
	end

end