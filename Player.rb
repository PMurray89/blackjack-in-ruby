require_relative 'Card.rb'
require_relative 'Hand.rb'
require_relative 'Strategy.rb'

##
# This class holds the key data on the player and her hands
class Player
	attr_reader :name, :bankroll
	
	# Initialize the player with a name and some starting amount
	def initialize(name, startingMoney = 1000)
		@name = name
		@bankroll = startingMoney
		@currentInsuranceBet = 0
		@hands = Array.new
	end
	
	# Providing an iterator for outside access of the hands. Due to the 
	# intimate relationships between the dealer, players, shoe and hands,
	# a fully encapsulated approach would be difficult. This method allows
	# the managing BlackJackGame class to add cards directly to the hands.
	def eachHand
		return @hands.each { |hand| yield hand }
	end	
	
	# Creates the first hand of the round. First deletes any existing hands.
	def createFirstHand(card1, card2, bet)
		@currentInsuranceBet = 0
		@hands.clear
		hand = Hand.new
		hand.addCard(card1)
		hand.addCard(card2)
		@bankroll -= bet
		hand.setBet(bet)
		@hands.push(hand)
		return @hands[0].to_s
	end
	
	# Splits the given hand
	def splitHand(hand)
		newHand = hand.split
		extraBet = hand.getBet
		@bankroll -= extraBet
		newHand.setBet(extraBet)
		@hands.push(newHand)
	end
	
	# Defers to human_strategy to decide what move to make on a given hand
	def decideMove(hand)
		return human_strategy(hand, @bankroll)
	end
	
	# Counts hand as a win and adds money to player's bankroll
	def winHand(hand)
		@bankroll += hand.win_payout
	end
	
	# Counts hand as a tie and returns money to player's bankroll
	def tieHand(hand)
		@bankroll += hand.tie_payout
	end
	
	# no loseHand method because money is already subtracted and stored in the hand's bet
	
	# Doubles the money on a given hand. For use when doubling down.
	def doubleBet(hand)
		bet = hand.getBet
		if @bankroll >= bet
			@bankroll -= bet
			hand.setBet(2*bet)
		else
			puts "Cannot match full bet. Went all in."
			hand.setBet(bet + @bankroll)
			@bankroll = 0
		end
	end
	
	# Gives the player the opportunity to buy insurance.
	def offerInsurance
		value = ((@hands[0].getBet)/2).ceil
		if value > @bankroll
			puts self.to_s + " does not have enough bankroll for insurance!"
		else
			puts "\n" + self.to_s + ", buy insurance for $#{value}? Y/N  Your hand is:\n" + @hands[0].to_s
			input = gets.chomp.upcase
			if input == 'Y'
				@bankroll -= value
				@currentInsuranceBet += value
			end
		end
	end
	
	# Player wins the insurance bet (if she placed one)
	def payoutInsurance
		if @currentInsuranceBet != 0
			puts @name + " won $#{2*@currentInsuranceBet} back from insurance!"
			@bankroll += 3*@currentInsuranceBet		# insurace pays 2:1, but the bet was already 'paid', so you are returned 3 X as much
			@currentInsuranceBet = 0
		end
	end
	
	# Prints the player's name
	def to_s
		return @name
	end

end
