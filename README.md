# README file. Learning Ruby for LiveRamp interview.
# Run Main.rb to play game. Other files contain class definitions.

Current rules:

-	Splitting is available when a hand has exactly two cards of the same value, not rank, i.e. one can split Q J.
-	Unlimited splitting is available.
-	No Surrender
-	Can double down on any 2-card hand. A double down doubles your current bet, unless you cannot due to insufficient funds. In that case, it puts you all in.
-	Insurance pays 2:1, and is offered as a sidebet of 1/2 rounded up of your original bet.
-	Blackjack wins 3:2. Ex: You have $100 and bet $10. After winning with a blackjack, you have $115.
-	Normal bets are 1:1. Ex: You have $100 and bet $10. After winning/losing, you have $110/$90.
-	Ties result in returning money, except blackjack (Ace + 10pt on an unsplit hand) beats other 21 pt hands.
-	A player bust is an instant loss, regardless of whether the dealer busts (hence the house edge).
-	No-limit betting.


Dealer rules:

-	Dealer checks for blackjack immediately upon dealing hands. In this case, all non-blackjack player hands are forfeit.
-	Dealer cannot split, double down, surrender, or buy insurance. Her only options are hitting and standing.
-	Dealer hits on hard/soft 16 and below, and stays on hard/soft 17 and above.
-	Dealer blackjacks only win 1:1



Known Issues:

-	Several abort statements for incorrect game positions. Most should be unreachable, but should include some error catching (check if Ruby has try/catch statements?)	