##
# This program manages the BlackJackGame class to start and
# run a simple command line interface game of blackjack between
# a dealer and multiple humans.
# Author::	Patrick Murray	(patrickmurray89@gmail.com)
 

# Checks for version of ruby. Versions prior to 1.9.2 should use 'require' rather
# than 'require_relative'
version = RUBY_VERSION.split('.')
if version[1] < '9' or (version[2] == '9' and version[2] < '2')
	def require_relative(file)
		require file
	end
end

require_relative 'BlackJackGame.rb'

puts "Welcome to Black Jack! How many decks should we use?"
numDecks = gets.chomp.to_i
while numDecks < 1
	puts "Must have at least one deck. How many should we use?"
	numDecks = gets.chomp.to_i
end
game = BlackJackGame.new(numDecks)

puts "How many human players do we have?"
numPlayers = gets.chomp.to_i
while numPlayers < 1 or numPlayers > 5*numDecks
	puts "Must have at least one human player, and no more than 5 players per deck (#{5*numDecks} players). How many players are there?"
	numPlayers = gets.chomp.to_i
end
puts "Enter the names of each player."
numPlayers.times do
	game.addPlayer(gets.chomp)
end

secondRound = 'Y'
while secondRound != 'N'
	unless game.playRound
		abort('End of game!')
	end
	puts "Play another round? Y/N"
	secondRound = gets.chomp.upcase
end
puts "Goodbye!"